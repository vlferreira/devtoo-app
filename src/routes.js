import {Router} from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import UserController from './app/controllers/userController';
import SessionController from './app/controllers/sessionController';
import FileController from './app/controllers/fileController';
import TextPostController from './app/controllers/textPostController';
import CommentsController from './app/controllers/commentsController';

import authMiddleware from './app/middlewares/auth';

const routes = new Router();
const upload = multer(multerConfig);

routes.post('/users', UserController.store);
routes.post('/sessions', SessionController.store);
routes.use(authMiddleware);

routes.post('/files', upload.single('file'), FileController.store);

routes.get('/users', UserController.index);
routes.put('/users', UserController.update);

routes.get('/textpost', TextPostController.index);
routes.get('/textpost/search', TextPostController.search);
routes.post('/textpost', TextPostController.store);
routes.put('/textpost', TextPostController.update);
routes.get('/textpost/:postId', TextPostController.show);
routes.put('/textpost/delete', TextPostController.delete);


routes.post('/comments', CommentsController.store);
routes.get('/comments/:postId', CommentsController.index);

export default routes;