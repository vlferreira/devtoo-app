import Sequelize from 'sequelize';
import databaseConfig from '../config/database';

import Users from '../app/models/users';
import Files from '../app/models/files';
import TextPost from '../app/models/textpost';
import Comments from '../app/models/comments';

const models = [Users, Files, TextPost, Comments];

class Database{
    constructor(){
        this.init();
    }

    init(){
        this.connection = new Sequelize(databaseConfig);

        models.map(model => model.init(this.connection)).map(model => model.associate && model.associate(this.connection.models));
    }
}

export default new Database();