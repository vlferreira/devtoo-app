'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('comments', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            main_comment_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 0
            },
            user_id: {
                type: Sequelize.INTEGER,
                reference: {model: 'users', key: 'id'},
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
                allowNull: false
            },
            reference_post_id: {
                type: Sequelize.INTEGER,
                reference: {model: 'textPost', key: 'id'},
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
                allowNull: false
            },
            text_content: {
                type: Sequelize.STRING,
                allowNull: false
            },
            is_valid: {
                type: Sequelize.BOOLEAN,
                defaultValue: true,
                allowNull: false
            },
            votes: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
                allowNull: false
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('comments');
    }
};