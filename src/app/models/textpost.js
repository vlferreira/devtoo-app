import Sequelize, {Model} from 'sequelize';

class TextPost extends Model{
    static init(sequelize){
        super.init(
            {
                text_type: Sequelize.INTEGER,
                title: Sequelize.STRING,
                text_content: Sequelize.STRING,
                is_valid: Sequelize.BOOLEAN
            },
            {
                sequelize
            }
        );

        return this;
    }

    static associate(models) {
        this.belongsTo(models.Users, {foreignKey: 'user_id', as: 'author'});
    }
}

export default TextPost;