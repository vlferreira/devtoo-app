import * as Yup from 'yup';
import Sequelize, {Model} from 'sequelize';

class Comments extends Model{
    static init(sequelize){
        super.init(
            {
                text_content: Sequelize.STRING,
                is_valid: Sequelize.BOOLEAN,
                votes: Sequelize.INTEGER
            },
            {
                sequelize
            }
        );

        return this;
    }

    static associate(models) {
        this.belongsTo(models.Users, {foreignKey: 'user_id', as: 'author'});
        this.belongsTo(models.Files, {foreignKey: 'user_id', as: 'avatar'});
        this.belongsTo(models.TextPost, {foreignKey: 'reference_post_id'});
    }
}

export default Comments;