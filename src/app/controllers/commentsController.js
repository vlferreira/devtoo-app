import * as Yup from 'yup';
import Comments from '../models/comments';
import Users from '../models/users';
import Files from '../models/files';
import TextPost from '../models/textpost';

class CommentsController{
    async index(req, res){
        const { page = 1 } = req.query;
        
        const comments = await Comments.findAndCountAll({
            where: {reference_post_id: req.params.postId, is_valid: true},
            order: ['votes', 'created_at'],
            attributes: ['id', 'user_id', 'text_content', 'votes', 'created_at'],
            limit: 10,
            offset: (page - 1) * 10,
            include: [
                {
                    model: Users,
                    as: 'author',
                    attributes: ['id', 'name']
                },
                {
                    model: Files,
                    as: 'avatar',
                    attributes: ['id', 'path', 'url']
                }
            ]
        });

        return res.json({
            "comments": comments.rows,
            "count": comments.count
        });
    }

    async store(req, res) {
        const schema = Yup.object().shape({
            text_content: Yup.string().required(),
            user_id: Yup.number().required(),
            reference_post_id: Yup.number().required()
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validação falhou.'});
        }

        const textPost = await TextPost.findByPk(req.body.reference_post_id);
        
        if(textPost.user_id != req.user_id){
            const user = await Users.findByPk(textPost.user_id);
        }

        const comments = await Comments.create(req.body);        

        return res.json(comments);
    }

    async update(req, res){
       
        if(req.body.text_content){
            const schema = Yup.object().shape({
                text_content: Yup.string().required()
            });
    
            if(!(await schema.isValid(req.body))){
                return res.status(400).json({error: 'Validação falhou.'});
            }
        }

        const comments = await Comments.findByPk(req.body.id);

        const response = await comments.update(req.body);
        
        return res.json(response);
    }

    async delete(req, res){             
        const schema = Yup.object().shape({
            text_content: Yup.string().required(),
            user_id: Yup.integer().required(),
            reference_post_id: Yup.integer().required()
        });

        req.body.is_valid = false;

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validação falhou.'});
        } 

        if(req.userIdLogged != req.user_id){
            return res.status(400).json({error: 'O usuário não pode remover este comentário.'});
        }

        const comments = await Comments.findByPk(req.body.id);

        const response = await comments.update(req.body);
        
        return res.json(response);
    }
}

export default new CommentsController();