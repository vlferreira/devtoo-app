import * as Yup from 'yup';
import Users from '../models/users';
import Files from '../models/files';

class UserController{
    async index(req, res){
        const { page = 1 } = req.query;
        const users = await Users.findAll({
            where: {name: req.name, email: req.email},
            order: ['name'],
            attributes: ['name'],
            limit: 20,
            offset: (page - 1) * 20,
            include: [
                {
                    model: Files,
                    as: 'avatar',
                    attributes: ['id', 'path', 'url']
                }
            ]
        });
    }

    async store(req, res) {
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string().email().required(),
            password: Yup.string().required().min(6)
        });
        
        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validação falhou.'});
        }

        const userExists = await Users.findOne({
            where: {
                email: req.body.email
            }
        });
        
        if(userExists){
            return res.status(400).json({
                error: 'O usuário já existe.'
            });
        }
        const user = await Users.create(req.body);
        
        return res.json(user);
    }

    async update(req, res){
        const schema = Yup.object().shape({
            name: Yup.string(),
            email: Yup.string().email(),
            oldPassword: Yup.string().min(6),
            password: Yup.string().min(6).when('oldPassword', (oldPassword, field) => oldPassword ? field.required() : field),
            confirmPassword: Yup.string().when('password', (password, field) =>
                password ? field.required().oneOf([Yup.ref('password')]) : field
            )
        });
        
        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validação falhou.'});
        }

        const {email, oldPassword} = req.body;
        const user = await Users.findByPk(req.id);

        if(email != user.email){
            const userExists = await Users.findOne({
                where: {email}
            });

            if(userExists){
                return res.status(400).json({error: 'O usuário já existe.'});
            }
        }

        if(oldPassword && !(await user.checkPassword(oldPassword))){
            return res.status(401).json({error: 'Senha atual incorreta.'});
        }

        await user.update(req.body);

        const {id, name, avatar} = await Users.findByPk(req.id, {
            include: [
                {
                    model: Files,
                    as: 'avatar',
                    attributes: ['id', 'path', 'url']
                }
            ]
        })

        return res.json({
            id,
            name,
            email,
            avatar
        });
    }
}

export default new UserController();