import * as Yup from 'yup';
import TextPost from '../models/textpost';
import Users from '../models/users';
import { Op } from 'sequelize';

class TextPostController{
    async index(req, res){
        const { page = 1 } = req.query;

        if(req.query.userId){
            const textPost = await TextPost.findAll({
                where: { user_id: req.query.userId, is_valid: true },
                order: [['created_at', 'DESC']],
                attributes: ['id', 'title', 'text_content', 'text_type', 'created_at'],
                limit: 20,
                offset: (page - 1) * 10,
                include: [
                    {
                        model: Users,
                        as: 'author',
                        attributes: ['id', 'name']
                    }
                ]
            });

            return res.json(textPost);
        } else {
            const textPost = await TextPost.findAll({
                where: { is_valid: true },
                order: [['created_at', 'DESC']],
                attributes: ['id', 'title', 'text_content', 'text_type', 'created_at'],
                limit: 20,
                offset: (page - 1) * 10,
                include: [
                    {
                        model: Users,
                        as: 'author',
                        attributes: ['id', 'name']
                    }
                ]
            });

            return res.json(textPost);
        }
    }

    async show(req, res){
        const textPost = await TextPost.findOne({
                where: { id: req.params.postId, is_valid: true },
                attributes: ['id', 'title', 'text_content', 'user_id', 'text_type', 'created_at'],
                include: [
                    {
                        model: Users,
                        as: 'author',
                        attributes: ['id', 'name']
                    }
                ]
        });

        return res.json(textPost);
    }

    async search(req, res){
        const { page = 1 } = req.query;
        let orderBy = null;
        let orderByLabel;
        let textType;

        if(req.query.order == 1){
            orderBy = 'DESC';
            orderByLabel = 'created_at';
        } else if(req.query.order == 2){
            orderBy = 'ASC';
            orderByLabel = 'created_at';
        }

        if(req.query.textType == 1){
            textType = [1, 2];
        } else if(req.query.textType == 2){
            textType = [1];
        } else if(req.query.textType == 3){
            textType = [2];
        }

        const textPost = await TextPost.findAll({
            where: {[Op.or]: 
                    [
                        {title: {[Op.substring]: req.query.textSearch}}, 
                        {text_content: {[Op.substring]: req.query.textSearch}},
                    ],
                    is_valid: true,
                    text_type: {[Op.in]: textType}
                },
            order: [[orderByLabel, orderBy]],
            attributes: ['id', 'title', 'text_content', 'text_type', 'created_at'],
            limit: 20,
            offset: (page - 1) * 10,
            include: [
                {
                    model: Users,
                    as: 'author',
                    attributes: ['id', 'name']
                }
            ]
        });

        return res.json(textPost);
    }

    async delete(req, res){             
        const schema = Yup.object().shape({
            is_valid: Yup.boolean().required(),
            user_id: Yup.number().required(),
            userIdLogged: Yup.number().required()
        });

        req.body.is_valid = false;

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validação falhou.'});
        } 

        if(req.userIdLogged != req.user_id){
            return res.status(400).json({error: 'O usuário não pode remover esta postagem.'});
        }

        const textPost = await TextPost.findByPk(req.body.id);

        const response = await textPost.update(req.body);
        
        return res.json(response);
    }

    async store(req, res) {
        const schema = Yup.object().shape({
            text_type: Yup.number().required(),
            title: Yup.string().required(),
            text_content: Yup.string().required(),
            user_id: Yup.number().required()
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validação falhou.'});   
        }

        const textPost = await TextPost.create(req.body);

        return res.json(textPost);
    }

    async update(req, res){
        const schema = Yup.object().shape({
            title: Yup.string().required(),
            text_content: Yup.string().required()
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validação falhou.'});
        }

        const textPost = await TextPost.findByPk(req.body.id);

        const response = await textPost.update(req.body);
        
        return res.json(response);
    }
}

export default new TextPostController();