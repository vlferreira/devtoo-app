const express = require('express');

const server = express();
server.use(express.json())

const users = ['teste1', 'teste2', 'teste3'];

//Midleware global
server.use((req, res, next) => {
    console.log(`Método: ${req.method}`);
    console.log(`URL: ${req.url}`);

    return next();
});

//Midleware local
function checkcUserExists(req, res, next){
    if(!req.body.name){
        return res.status(400).json({error: 'Usuário não encontrado'});
    }

    return next();
}

//Lista de um usuário
server.get('/users/:id', (req, res) => {
    const {id} = req.params;
    return res.json(users[id]);
});

//Lista de Usuários
server.get('/users', (req, res) => {
    return res.json(users);
});

server.post('/users', checkcUserExists, (req, res) =>{
    const {name} = req.body;
});

server.put('/users/:id', checkcUserExists, (req, res) => {
    const {id} = req.params;
    const {name} = req.body;
});

server.delete('/users/:id', (req, res) => {
    const {id} = req.params;
});

server.listen(3000);